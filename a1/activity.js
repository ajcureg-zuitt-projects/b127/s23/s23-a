// Create a single room

/*

db.users.insertOne({
    "name": "single",
    "accomodates": "2",
    "price": 1000,
    "description": "A simple room with all the basic necessities."
});


db.users.updateOne(
{
    "_id": ObjectId("6136020e16eb1229721fcce2")
},
{
    $set: {
        "rooms_available": "10",
        "isAvailable": "false"
    }
}
);

*/




/*

db.users.insertMany([
{
    "name": "queen",
    "accomodates": "4",
    "price": 4000,
    "description": "A room with a queen sized bed perfect for a simple getaway.",
    "rooms_available": "15",
    "isAvailable": "false"
},
{
    "name": "double",
    "accomodates": "3",
    "price": 2000,
    "description": "A room fit for a small family going on a vacation.",
    "rooms_available": "5",
    "isAvailable": "false"
}
]);

*/



/*

//Number 5.
db.users.find({ "name": "double" });


*/



/*

//Number 6.
db.users.updateOne(
{
    "_id": ObjectId("6136059616eb1229721fcce3")
},
{
    $set: {
        "rooms_available": "0"
    }
}
);

*/


/*

//Number 7.
db.users.deleteMany({ "rooms_available": "0" });

*/